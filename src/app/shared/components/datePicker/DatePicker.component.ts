import { Component, Input, ElementRef, AfterContentInit, OnDestroy, forwardRef, ViewEncapsulation, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import * as Flatpickr from 'flatpickr';


@Component({
  selector: 'dcs-date-picker',
  templateUrl: './DatePicker.tpl.html',
  encapsulation: ViewEncapsulation.None, // flatpickr pushes the picker outside of the component
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DatePickerComponent),
      multi: true
    }
  ]
})
export class DatePickerComponent implements AfterContentInit, OnDestroy, ControlValueAccessor {
  @Input() inputId: string;
  @Input() placeholder: string = 'Click to select date';
  @Input() options: any = {};
  @ViewChild('flatpickr') inputReference: ElementRef;

  datePicker: any;
  defaultOptions: any = {
    time_24hr: true,
    altFormat: 'j.n.Y',
    altInput: true,
    altInputClass: 'form-control',
    onChange: this.onChange.bind(this)
  };
  _value: Date;

  propagateChange = (_: any) => {};


  get dateInput(): HTMLInputElement {
    return this.inputReference.nativeElement;
  }

  get pickerOptions(): any {
    return Object.assign({}, this.defaultOptions, this.options);
  }

  ngAfterContentInit() {
    this.datePicker = new Flatpickr(this.dateInput, this.pickerOptions);
    if (this._value) {
      this.datePicker.setDate(this._value);
    }
  }

  ngOnDestroy() {
     this.datePicker.destroy();
  }

  writeValue(value: Date) {
    value = new Date(value);

    if (isNaN(value.getTime())) {
      // empty initial value, bail out early
      return;
    }

    if (this.datePicker) {
      this.datePicker.setDate(value);
    } else {
      this._value = value;
    }
  }

  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  registerOnTouched() {}

  onChange(date: Date) {
    this.propagateChange(date);
  }

}
