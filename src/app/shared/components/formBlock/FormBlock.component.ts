import { Component, Input } from '@angular/core';


@Component({
  selector: 'dcs-form-block',
  templateUrl: './FormBlock.tpl.html'
})
export class FormBlockComponent {

  @Input() hasError: boolean;

}
