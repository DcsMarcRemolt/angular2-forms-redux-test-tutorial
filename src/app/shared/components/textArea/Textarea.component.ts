import { Component, Input, ElementRef, AfterViewInit, OnDestroy, forwardRef, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import * as Autogrow from 'textarea-autogrow';


@Component({
  selector: 'dcs-textarea',
  templateUrl: './Textarea.tpl.html',
  styles: [
    `
    textarea.autogrow {
      resize: none;
    }
    `
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextareaComponent),
      multi: true
    }
  ]
})
export class TextareaComponent implements AfterViewInit, OnDestroy, ControlValueAccessor {

  @Input() rows: number;
  @Input() idValue: string;
  @Input() placeholder: string;
  @ViewChild('inputField') inputRef: ElementRef;

  propagateChange = (_: any) => { };
  autogrow: any;
  onChange$: Observable<string>;
  onChangeSubscription: Subscription;
  userIsEditing: boolean = false;
  userIsEditingTimeout: any;

  get input(): HTMLTextAreaElement {
    return this.inputRef.nativeElement;
  }

  ngAfterViewInit() {
    this.autogrow = new Autogrow(this.input);
    this.autogrow.autogrowFn();

    this.onChange$ = Observable
      .merge(
        Observable.fromEvent(this.input, 'change'),
        Observable.fromEvent(this.input, 'keyup'),
      )
      .map((event: any) => event.target.value)
      .distinctUntilChanged();

    this.onChangeSubscription = this.onChange$.subscribe((value: string) => {
      this.userIsEditing = true;

      // WTF Angular, no number returned from setTimeout????
      if (this.userIsEditingTimeout) {
        clearTimeout(this.userIsEditingTimeout.data.handleId);
      }

      this.userIsEditingTimeout = setTimeout(() => {
        this.userIsEditing = false;
      }, 1000);
      this.propagateChange(value);
    });
  }

  ngOnDestroy() {
    this.onChangeSubscription.unsubscribe();
  }

  writeValue(value: string) {
    // do not update the input field during user editing, bad timing issues, vanishing letters ...
    // this is not necessary anyway, as the user types anyway, so updates from store do not male sense
    if (!this.userIsEditing) {
      this.input.value = value;
    }
  }

  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  registerOnTouched() { }

}
