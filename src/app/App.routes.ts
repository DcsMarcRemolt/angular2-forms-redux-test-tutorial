import { provideRoutes, Route } from '@angular/router';

import { HomeComponent } from './home/Home.component';
import { NotFoundComponent } from './notFound/NotFound.component';


export const routes: Array<Route> = [
  { path: '', component: HomeComponent },
  { path: '**', component: NotFoundComponent },
];

export const appRouterProviders = [
  provideRoutes(routes)
];
