import { Component } from '@angular/core';

@Component({
  selector: 'dcs-home',
  templateUrl: './Home.tpl.html',
  styles: [
    `
      h1 { color: red; }
    `
  ]
})
export class HomeComponent {
  who: string = 'World';
}
