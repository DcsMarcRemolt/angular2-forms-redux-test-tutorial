import { OpaqueToken } from '@angular/core';
import { combineReducers } from 'redux-immutable';
import { fromJS } from 'immutable';

import { IAction, IReducer, IState } from '../shared/interfaces';


// make Redux heppy, remove when first real reducer is added
const initialState: IState = fromJS({
  foo: 'bar'
});

function fooReducer(state: IState = initialState, action: IAction): IState {
  return state;
}


export const RootReducer: OpaqueToken = new OpaqueToken('RootReducer');

export const rootReducer: IReducer = combineReducers({
  foo: fooReducer
});
