import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { routes } from './App.routes';

import { HomeComponent } from './home/Home.component';
import { NotFoundComponent } from './notFound/NotFound.component';

import { SharedModule } from './shared/Shared.module';


@NgModule({
  declarations: [
    // components
    HomeComponent,
    NotFoundComponent,
  ],
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true
    }),
    SharedModule
  ]
})
export class AppModule { }
